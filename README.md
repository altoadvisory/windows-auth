windows-auth

Tool to test Windows Authentication.
Just run it and browse to the indicated URL: if test is OK, user should see his/her Active Directory sid and groups displayed.

Please note that this will also work with if the host is not joined to a domain.
In that case, this will list the local groups the connected user belongs to.

Initialize with 'npm i'
Run with 'node index.js'