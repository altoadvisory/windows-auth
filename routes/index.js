var express = require('express')
var router = express.Router()

router.get('/',function(req, res) {           
    if(req.isAuthenticated()) {
        let out = 'Hello ' + req.user.logon + '!<br/>Your sid is ' + req.user.sid + '<br/>'
        if (req.user.groups) {
            out+='Your groups are:<br/><ul>'
            for (let g of req.user.groups) {
                out += '<li>' + g + '</li><br/>'
            }
            out += '</ul>'
        }
        res.send(out)
    }
    else res.redirect('/login')
})

module.exports = router;