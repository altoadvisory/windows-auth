module.exports = function(passport) {
    const express = require('express')
    const router = express.Router()
    
    router.get('/',  passport.authenticate('sspi', { 
        successRedirect: '/',
        failureRedirect: '/fail'
    }))

    return router
}