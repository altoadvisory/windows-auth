const express = require('express')
const bodyParser = require('body-parser')
const session = require('express-session');
const passport = require('passport')
const SSPIStrategy = require('alto-passport-sspi').Strategy
const nodeSSPI = require('node-sspi')

const sspi = new SSPIStrategy(
  {
    retrieveGroups : true
  },
  function(user, done) {
    return done(null,user)
  }
)
passport.use(sspi)

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

function sspiMiddleware(req,res,next) {
    const nodeSSPIObj = new nodeSSPI({
    // contrary to defaults, we do not want Basic auth to be offered. If Windows-auth fails, it's the end.
    // perRequestAuth, retrieveGroups, maxLoginAttemptsPerConnection are exposed
    // most of the time perRequestAuth and maxLoginAttemptsPerConnection should be left alone
    offerBasic: false,
    perRequestAuth: (sspi.perRequestAuth===true) ? true:false,
    retrieveGroups: (sspi.retrieveGroups===true) ? true:false,
    maxLoginAttemptsPerConnection: (typeof sspi.maxLoginAttemptsPerConnection==='number') ? sspi.maxLoginAttemptsPerConnection:3
  })
  nodeSSPIObj.authenticate(req, res, function(){
    res.finished || next()
  })  
}

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(session({
  secret: 'secret',
  saveUninitialized: false,
  resave: false
}))
app.use(passport.initialize())
app.use(passport.session())

app.use('/', require('./routes/index'))
app.use('/login', sspiMiddleware, require('./routes/login')(passport))
app.use('/fail', require('./routes/fail'))

app.listen(3200, function () {
  console.log('Listening on port 3200!')
})
